﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UdemyCodeFirstDBConnection.Startup))]
namespace UdemyCodeFirstDBConnection
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
