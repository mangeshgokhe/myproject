﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UdemyCodeFirstDBConnection.CustomerDTO;
using UdemyCodeFirstDBConnection.Models;

namespace UdemyCodeFirstDBConnection.App_Start
{
    public class MappingProfle : Profile
    {
        public MappingProfle()
        {
            Mapper.CreateMap<CustomerModel,CustomersDTO>();
            Mapper.CreateMap<CustomersDTO, CustomerModel>();
        }
    }

}