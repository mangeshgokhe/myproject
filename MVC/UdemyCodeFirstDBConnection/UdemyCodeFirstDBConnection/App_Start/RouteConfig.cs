﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UdemyCodeFirstDBConnection
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();
            //routes.MapRoute(
            //     "MoviesList",
            //     "{controller}/{action}/{year}/{month}",
            //     new { controller = "Movies", action = "Custom", year = @"\d{4}", month = @"\d{2}" }
            //     );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "RandomMovieList",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Movies", action = "RandomMovieList", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                          name: "CustomerList",
                          url: "{controller}/{action}/{id}",
                          defaults: new { controller = "Customers", action = "Index", id = UrlParameter.Optional }
                      );
        }
    }
}
