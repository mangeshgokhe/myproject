﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UdemyCodeFirstDBConnection.CustomerDTO
{
    public class CustomersDTO
    {
        public int CustID { get; set; }
        [Required]
        [StringLength(255)]
        public string CustName { get; set; }
        [Display(Name = "Date of Birth:")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BirthDate { get; set; }
        public bool IsSubscribedToNewsletter { get; set; }
        public byte MembershipTypeId { get; set; }

    }
}