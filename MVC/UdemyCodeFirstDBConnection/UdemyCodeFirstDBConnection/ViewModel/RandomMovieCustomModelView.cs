﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UdemyCodeFirstDBConnection.Models;

namespace UdemyCodeFirstDBConnection.ViewModel
{
    public class RandomMovieCustomModelView
    {
        public Movies Movieee { get; set; }
        public List<CustomerModel> Cust { get; set; }
    }
}