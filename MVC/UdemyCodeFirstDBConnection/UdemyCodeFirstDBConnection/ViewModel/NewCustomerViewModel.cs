﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UdemyCodeFirstDBConnection.ViewModel;
using UdemyCodeFirstDBConnection.Models;

namespace UdemyCodeFirstDBConnection.ViewModel
{
    public class NewCustomerViewModel
    {
        public List<MembershipType> MembershipTypes { get; set; }
        public CustomerModel CustomerModels { get; set; }
    }
}