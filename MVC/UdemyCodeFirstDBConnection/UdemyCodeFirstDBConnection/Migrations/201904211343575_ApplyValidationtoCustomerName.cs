namespace UdemyCodeFirstDBConnection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplyValidationtoCustomerName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CustomerModels", "CustName", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CustomerModels", "CustName", c => c.String());
        }
    }
}
