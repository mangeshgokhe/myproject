namespace UdemyCodeFirstDBConnection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembershipTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT into MembershipTypes(id,SignUpFee,DurationInMonths,DiscountRate) values(1,0,0,0)" );
            Sql("INSERT into MembershipTypes(id,SignUpFee,DurationInMonths,DiscountRate) values(2,30,1,0.15)");
            Sql("INSERT into MembershipTypes(id,SignUpFee,DurationInMonths,DiscountRate) values(3,90,2,1.20)");
            Sql("INSERT into MembershipTypes(id,SignUpFee,DurationInMonths,DiscountRate) values(4,3,3,1.2)");
        }
        
        public override void Down()
        {
        }
    }
}
