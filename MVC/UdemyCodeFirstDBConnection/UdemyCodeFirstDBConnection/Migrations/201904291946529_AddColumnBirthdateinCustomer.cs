namespace UdemyCodeFirstDBConnection.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnBirthdateinCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomerModels", "BirthDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomerModels", "BirthDate");
        }
    }
}
