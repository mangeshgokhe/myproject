﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdemyCodeFirstDBConnection.Models;
using UdemyCodeFirstDBConnection.ViewModel;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace UdemyCodeFirstDBConnection.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _Context;

        public CustomersController()
        {
            _Context = new ApplicationDbContext();  
        }
        protected override void Dispose(bool Disposing)
        {
            _Context.Dispose();
        }
        // GET: Customers
        [Authorize]
        public ActionResult Index()
        {
            var cust = new List<CustomerModel>
            {
                new CustomerModel {CustID=1,CustName="Mangesh"},
                new CustomerModel {CustID=2,CustName="Vishal"}

            };
                 cust = _Context.CustomerMode.Include(c =>c.MembershipType).ToList();
            return View(cust);
        }
        public ActionResult CustomerForm()
        {
            var MembershipType = _Context.MembershipTypes.ToList();
            var ViewModel = new NewCustomerViewModel()
            {
                MembershipTypes = MembershipType
            };
            return View("CustomerForm",ViewModel);
        }
        [HttpPost]
        public ActionResult Save(NewCustomerViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                var ModelView = new NewCustomerViewModel()
                {
                    CustomerModels = ViewModel.CustomerModels,
                    MembershipTypes = _Context.MembershipTypes.ToList()

                };
                return View("CustomerForm", ModelView);

            }
            if(ViewModel.CustomerModels.CustID==0)
            _Context.CustomerMode.Add(ViewModel.CustomerModels);
            else
            {
                var CustomerInDB = _Context.CustomerMode.Single(c => c.CustID == ViewModel.CustomerModels.CustID);
                CustomerInDB.CustName = ViewModel.CustomerModels.CustName;
                CustomerInDB.IsSubscribedToNewsletter = ViewModel.CustomerModels.IsSubscribedToNewsletter;
                CustomerInDB.BirthDate = ViewModel.CustomerModels.BirthDate;
                CustomerInDB.MembershipTypeId = ViewModel.CustomerModels.MembershipTypeId;
            }
            try
            {
                _Context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("Index","Customers");
        }

        [Authorize]
        public ActionResult Details(int id)
        {

            //var cust = new List<CustomerModel>
            //{
            //    new CustomerModel {CustID=1,CustName="Mangesh"},
            //    new CustomerModel {CustID=2,CustName="Vishal"}

            //};
            var Customers = _Context.CustomerMode.Include(c => c.MembershipType).SingleOrDefault(c => c.CustID == id);
            if (Customers == null)
                return HttpNotFound();
            return View(Customers);
        }

        public ActionResult Edit(int id)
        {
            var Customers = _Context.CustomerMode.SingleOrDefault(c => c.CustID == id);
            if (Customers == null)
                return HttpNotFound();

            var ViewModel = new NewCustomerViewModel
            {
                CustomerModels=Customers,
                MembershipTypes=_Context.MembershipTypes.ToList()
            };
            
            return View("CustomerForm",ViewModel);
        }
        public ActionResult Delete(int id)
        {
            var Customers = _Context.CustomerMode.SingleOrDefault(c => c.CustID == id);
            if (Customers == null)
                return HttpNotFound();

            _Context.CustomerMode.Remove(Customers);
            try
            {
                _Context.SaveChanges();
            }
            catch(DbEntityValidationException e)
            {
                Console.WriteLine(e);
            }

            return RedirectToAction("Index", "Customers");
        }
    }


}