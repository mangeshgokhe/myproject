﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UdemyCodeFirstDBConnection.Models;
using UdemyCodeFirstDBConnection.ViewModel;

namespace UdemyCodeFirstDBConnection.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Index()
        {
            var objMovie = new Movies();
            //objMovie.MovieID = 1;
            objMovie.MovieName="Ready";
            return View(objMovie);
        }

        public ActionResult RandomMovieList()
        {
            var objMovie = new Movies(){MovieName="Ready"};


           var cust = new List<CustomerModel>
            {
                new CustomerModel{CustName="Mangesh"},
                new CustomerModel {CustName="Vishal"}
            };

           var ListCust = new RandomMovieCustomModelView()
           {
               Movieee = objMovie,
               Cust = cust
           };


           return View(ListCust);
        }
        //For attribute routing
        [Route("Movies/Custom/{year:regex(\\d{2}):range(1995,2030)}/{month:regex(\\d{2}):range(1,12)}")]
        public ActionResult Custom(int year,int month)
        {
            return Content(year + "/" + month);
        }
    }
}