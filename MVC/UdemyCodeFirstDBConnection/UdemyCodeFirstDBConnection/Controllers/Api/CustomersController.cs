﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UdemyCodeFirstDBConnection.Models;
using UdemyCodeFirstDBConnection.ViewModel;
using UdemyCodeFirstDBConnection.CustomerDTO;
using AutoMapper;

namespace UdemyCodeFirstDBConnection.Controllers.Api
{   
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _Context = null;

        public CustomersController()
        {
            _Context = new ApplicationDbContext();
        }
        //GET /api/Cutomers
        public IEnumerable<CustomersDTO> GetCustomersList()
        {
            return _Context.CustomerMode.ToList().Select(Mapper.Map<CustomerModel,CustomersDTO>);
        }

        //GET /api/Customers/1
        public CustomersDTO GetCustomer(int id)
        {
            var customer = _Context.CustomerMode.SingleOrDefault(c => c.CustID == id);
            if (customer == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return Mapper.Map<CustomerModel,CustomersDTO>(customer);
        }

        //POST /api/Customers
        [HttpPost]
        public IHttpActionResult CreateCustomers(CustomerModel Customer)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            _Context.CustomerMode.Add(Customer);
            _Context.SaveChanges();
            return Created(new Uri(Request.RequestUri+"/"+Customer.CustID),Customer);
        }

        //PUT /api/Customers/1
        [HttpPut]
        public void UpdateCustomer(int id, CustomerModel Customer)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            var CustomerInDB = _Context.CustomerMode.SingleOrDefault(m => m.CustID == id);
            CustomerInDB.CustName = Customer.CustName;
            CustomerInDB.BirthDate = Customer.BirthDate;
            CustomerInDB.IsSubscribedToNewsletter = Customer.IsSubscribedToNewsletter;
            CustomerInDB.MembershipTypeId = Customer.MembershipTypeId;
            _Context.SaveChanges();
        }

        [HttpDelete]
        public void DeleteCustomer(int id)
        {
            var Customer = _Context.CustomerMode.SingleOrDefault(c => c.CustID == id);
            if (Customer == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            _Context.CustomerMode.Remove(Customer);
            _Context.SaveChanges();
        }
    

    }
}
