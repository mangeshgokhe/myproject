﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UdemyCodeFirstDBConnection.Models
{
    public class MembershipType
    {
        public byte ID { get; set; }
        public string MembershipName { get; set; }
        [Display(Name="Date of Birth") ]
        public DateTime BirthDate { get; set; }
        public short SignUpFee { get; set; }
        public byte DurationInMonths { get; set; }
        public byte DiscountRate { get; set; }

    }
}